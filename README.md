# Projet co-création API

TODO: description

## Getting started
First, and because there's a check for a database connection on startup you have to:
when running `development mode`
1. ```docker-compose up -d```
2. ```yarn dev```

*And be sure that there's no error in the logs!*

## geenral commands

Run dev environment:
```yarn dev```

Run build (transpiled in dist folder):
```yarn build```

Run lint:
```yarn lint```

Run lint for auto fix:
```yarn lint-fix```