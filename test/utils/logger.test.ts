/* eslint-disable @typescript-eslint/no-unused-vars */
const logger = {
  debug: jest.fn(),
  log: jest.fn(),
  add: jest.fn()
};

// trying to mock createLogger to return a specific logger instance
jest.mock('winston', () => ({
  format: {
    colorize: jest.fn(),
    combine: jest.fn(),
    label: jest.fn(),
    timestamp: jest.fn(),
    printf: jest.fn(),
    simple: jest.fn()
  },
  createLogger: jest.fn().mockReturnValue(logger),
  transports: {
    Console: jest.fn()
  }
}));

import * as winston from 'winston';
import { logger as applogger } from '../../src/utils/logger';

describe('-- Logging Service --', () => {
  let loggerMock: winston.Logger;

  test('testing logger log function called...', () => {
    const mockCreateLogger = jest.spyOn(winston, 'createLogger');
    const loggingService: typeof applogger = applogger;
    loggerMock = mockCreateLogger.mock.instances[0];
    // expect(loggingService).toBeInstanceOf(applogger);
    expect(loggingService).toBeDefined();
    expect(mockCreateLogger).toHaveBeenCalled();

    // spy on the winston.Logger instance within this test and check
    // that it is called - this is working from within the test method
    logger.log('debug', 'test log debug');
    expect(logger.log).toHaveBeenCalled();

    // now try and invoke the logger instance indirectly through the service class
    // check that loggerMock is called a second time - this fails, only called once
    // from the preceding lines in this test
    loggingService.debug('debug message');

    expect(logger.debug).toHaveBeenCalledTimes(1); // <- here
  });
});