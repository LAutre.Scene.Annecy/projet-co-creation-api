import { DataSource } from 'typeorm';

import { dbConfig, appConfig } from '../../src/config';
import { User } from '../../src/models/users-model';
import { Role } from '../../src/models/roles-model';

describe('Test config', () => {
  test('Test dbConfig', () => {
    expect(dbConfig).toBeInstanceOf(DataSource);
    expect(dbConfig.options).toEqual({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'test',
      password: 'password',
      database: 'cocreation',
      synchronize: false, // using true would overwrite tables defined in `entities`
      logging: true, // pass to false for production
      entities: [User, Role],
      migrations: [],
      subscribers: [],
    });
  });

  test('Test appConfig', () => {
    expect(appConfig).toEqual({
      secret: 'business cat'
    });
  });
});
