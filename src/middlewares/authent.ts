import express from 'express';

import { logger } from '../utils/logger';

export const authentController = (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const user = req.session.user;
  if (user && user.username !== '') {
    next();
  } else {
    logger.warn('Not authentified!');
    res.redirect('/login');
  }
};