import { defineConfig } from '../defineConfig';
import { Role } from '../../models/roles-model';
import { User } from '../../models/users-model';

export function createLocalConfig() {
  return defineConfig({
    db: {
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'user',
      password: 'labsoft',
      database: 'cocreation',
      synchronize: false, // using true would overwrite tables defined in `entities`
      logging: true, // pass to false for production
      entities: [User, Role],
      migrations: [],
      subscribers: [],
    },
    app: {
      secret: 'keyboard cat'
    }
  });
}
