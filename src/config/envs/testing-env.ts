import { defineConfig } from '../defineConfig';
import { Role } from '../../models/roles-model';
import { User } from '../../models/users-model';

export function createTestConfig() {
  return defineConfig({
    db: {
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'test',
      password: 'password',
      database: 'cocreation',
      synchronize: false, // using true would overwrite tables defined in `entities`
      logging: true, // pass to false for production
      entities: [User, Role],
      migrations: [],
      subscribers: [],
    },
    app: {
      secret: 'business cat'
    }
  });
}
