import { defineConfig } from '../defineConfig';

export function createProductionConfig() {
  return defineConfig({
    db: {
      host: '',
      port: 0,
      user: '',
      password: '',
      database: '',
    },
  });
}
