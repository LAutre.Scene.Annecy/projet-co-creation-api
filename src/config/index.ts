import 'reflect-metadata';
import { DataSource } from 'typeorm';

import { createLocalConfig } from './envs/local';
import { createProductionConfig } from './envs/production';
import { createTestConfig } from './envs/testing-env';

const localConf = createLocalConfig();
const testConf = createTestConfig();
const productionConf = createProductionConfig();

const getConfig = () => {
  switch (process.env.NODE_ENV) {
    case 'production':
      return new DataSource({ ...productionConf.db });
    case 'test':
      return new DataSource({ ...testConf.db });
    case 'development':
      return new DataSource({ ...localConf.db });
    default:
      throw new Error(`Invalid NODE_ENV "${process.env.NODE_ENV}"`);
  }
};

const getAppConfig = () => {
  switch (process.env.NODE_ENV) {
    case 'production':
      return productionConf.app;
    case 'test':
      return testConf.app;
    case 'development':
      return localConf.app;
    default:
      throw new Error(`Invalid NODE_ENV "${process.env.NODE_ENV}"`);
  }
};

export const dbConfig = getConfig();
export const appConfig = getAppConfig();
