export type sessionData = {
  username: string,
  email: string,
  role: string | undefined,
  organisation: string,
  last_login: Date
 }