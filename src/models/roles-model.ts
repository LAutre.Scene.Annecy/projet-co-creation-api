import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class Role {
  @PrimaryGeneratedColumn()
    id!: number;

  @Column('text')
    role!: string;

  @Column()
    creation_date!: Date;
}
