import { Entity, PrimaryGeneratedColumn, Column, OneToOne } from 'typeorm';

import { Role } from './roles-model';

@Entity()
export class User {
  @PrimaryGeneratedColumn()
    id!: number;

  @Column('text')
    username!: string;

  @Column('text')
    password!: string;

  @Column('text')
    email!: string;

  @OneToOne(() => Role) // TODO: to be resolved in a ticket: retrieve role name
  @Column()
    role!: number;

  @Column('text')
    organisation!: string;

  @Column()
    creation_date!: Date;

  @Column()
    last_login!: Date;
}
