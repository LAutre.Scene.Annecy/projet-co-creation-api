import express from 'express';
import session from 'express-session';
import createMemoryStore from 'memorystore';

import { appConfig } from './config';
import loginRoutes from './routes/login-route';
import logoutRoute from './routes/logout-route';
import type { sessionData } from './server';

const app = express();
const memorystore = createMemoryStore(session);

const isDev = process.env.NODE_ENV !== 'production';

app.use(session({
  secret: appConfig.secret,
  resave: false,
  saveUninitialized: true,
  cookie: {
    /**
     * httpOnly: true // note that for production
     * secure: true // note that for production
     * The server will also need a real certificate for https
     */
    httpOnly: !isDev,
    secure: !isDev,
    maxAge: 86400000,
  },
  store: new memorystore({ checkPeriod: 86400000 })
}));

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.static('public'));
app.set('view engine', 'pug');
app.set('views','views');

app.get('/', (req: express.Request, res: express.Response) => {
  const user = req.session.user as sessionData;
  if (user && user?.username !== '') {
    res.render('home', { username: req.session.user?.username, role: req.session.user?.role });
  } else {
    res.render('home');
  }
});

app.use(loginRoutes);
app.use(logoutRoute);

export default app;
