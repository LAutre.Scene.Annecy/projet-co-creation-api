import express from 'express';

import { logger } from '../utils/logger';
import { User } from '../models/users-model';
import { dbConfig } from '../config';

export const postLoginController = async (req: express.Request, res: express.Response) => {
  const inputEmail = req.body.inputEmail;
  const password = req.body.password;

  if (inputEmail && password) {
    try {
      const repository = dbConfig.getRepository(User);
      const user = await repository.findOneBy({ email: inputEmail });

      if (user && user.password === password) {
        req.session.user = user;
        req.session.save((err) => {
          if (err) {
            logger.error('Error while saving session', err);
          }
          res.redirect('/');
        });
      } else {
        logger.error('Wrong credentials, User not logged in');
        res.redirect('/login');
      }
    } catch (error) {
      logger.error('Error while fetching user', error);
      res.redirect('/login');
    }
  }
};