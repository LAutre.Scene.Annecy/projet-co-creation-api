import express from 'express';

import { logger } from '../utils/logger';

const logoutRoute = express.Router();

logoutRoute.get('/logout', (req: express.Request, res: express.Response) => {
  req.session.destroy((err) => {
    if (err) {
      logger.warn('logout failed', err);
      res.redirect('/');
    } else {
      res.redirect('/');
    }
  });
});

export default logoutRoute;