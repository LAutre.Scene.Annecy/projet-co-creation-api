import express from 'express';
import { z } from 'zod';

import { validate } from '../middlewares/validate';
import { postLoginController } from '../controllers/login-controllers';


const loginRoutes = express.Router();

const loginRouteSchema = z.object({
  body: z.object({
    inputEmail: z.string(),
    password: z.string(),
  })
});

loginRoutes.get('/login', (req: express.Request, res: express.Response) => {
  res.render('login');
});

loginRoutes.post('/login', validate(loginRouteSchema), postLoginController);

export default loginRoutes;