/* eslint-disable @typescript-eslint/no-unused-vars */

import app from './app';
import { logger } from './utils/logger';
import { User } from './models/users-model';
import { checkDatabaseConnection } from './utils/database-check';


const port = 8080;

app.listen(port, () => {
  return logger.log({ level: 'info', message: `http://localhost:${port}` });
});

// check if database is up and running
checkDatabaseConnection();

// Augment express-session with a custom SessionData object
export type sessionData = Omit<User, 'id' | 'password' | 'creation_date' | 'last_login'>;
declare module 'express-session' {
  interface SessionData {
    user: sessionData;
  }
} 