/* eslint-disable @typescript-eslint/no-unused-vars */
import winston, { format } from 'winston';
import util from 'util';

function transform(info: winston.Logform.TransformableInfo, opts: winston.Logform.JsonOptions) {
  const args = info[Symbol.for('splat')];
  if (args) {
    info.message = util.format(info.message, ...args);
  }
  return info;
}

function utilFormatter() {
  return { transform };
}

const logger = winston.createLogger({
  level: 'debug',
  format: format.combine(
    format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
    utilFormatter(),
    format.colorize(),
    format.printf(({ level, message, label, timestamp }) => `${timestamp} ${label || '-'} ${level}: ${message}`),
  ),
  defaultMeta: { service: 'cocreation-service' },
  transports: [
    //
    // - Write all logs with importance level of `error` or less to `error.log`
    // - Write all logs with importance level of `info` or less to `combined.log`
    //
    // new winston.transports.File({ filename: 'error.log', level: 'error' }),
    // new winston.transports.File({ filename: 'combined.log' }),
    // new winston.transports.Stream({
    //   stream: process.stderr,
    //   level: 'debug',
    // }),
  ],
});

if (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test') {
  logger.add(new winston.transports.Console({
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.simple(),
    )
  }));
}

export { logger };
