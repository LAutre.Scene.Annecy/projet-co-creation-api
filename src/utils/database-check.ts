import { dbConfig } from '../config';

import { logger } from './logger';

export const checkDatabaseConnection = () =>
  dbConfig
    .initialize()
    .then(() => {
      logger.info('Database connection successful');
      return true;
    })
    .catch((error: Error) => {
      logger.error('Database connection failed:', error);
      return false;
    });